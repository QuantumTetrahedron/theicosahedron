
#ifndef THEICOSAHEDRON_VULKANLOADER_H
#define THEICOSAHEDRON_VULKANLOADER_H

#define VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>
#include <vector>

namespace vk{
    bool LoadGlobalLevelFunctions();
    bool LoadInstanceLevelFunctions(const VkInstance& instance, const std::vector<char const *>& enabled_extensions);
    bool LoadDeviceLevelFunctions(const VkDevice& device, const std::vector<const char*>& enabled_extensions);
    void ReleaseLoader();

#define EXPORTED_VULKAN_FUNCTION( name ) extern PFN_##name name;
#define GLOBAL_LEVEL_VULKAN_FUNCTION( name ) extern PFN_##name name;
#define INSTANCE_LEVEL_VULKAN_FUNCTION( name ) extern PFN_##name name;
#define INSTANCE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) extern PFN_##name name;
#define DEVICE_LEVEL_VULKAN_FUNCTION( name ) extern PFN_##name name;
#define DEVICE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) extern PFN_##name name;

#include "VulkanLoader.inl"
}

#endif //THEICOSAHEDRON_VULKANLOADER_H
