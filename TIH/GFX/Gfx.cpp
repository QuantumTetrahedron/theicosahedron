
#include <iostream>
#include <cstring>
#include "Gfx.h"
#include "DeviceManager.h"

namespace TIH{
    VkInstance Gfx::instance;
    Window Gfx::window;

    bool Gfx::Initialize() {
        window.Init();

        if(!vk::LoadGlobalLevelFunctions()){
            std::cerr << "Failed to load global level functions" << std::endl;
            return false;
        }

        if(!CreateInstance()){
            return false;
        }

        if(!window.CreateSurface(instance)){
            return false;
        }

        if(!DeviceManager::Initialize(instance, window.surface)){
            return false;
        }


        return true;
    }

    bool Gfx::CreateInstance() {
        std::vector<VkExtensionProperties> instanceExtensions;

        if(!GetInstanceExtensions(instanceExtensions)){
            std::cerr << "Error while getting instance extensions" << std::endl;
            return false;
        }

        uint32_t desiredExtensionsCount = 0;
        const char** glfwExtensions = window.GetRequiredInstanceExtensions(&desiredExtensionsCount);

        std::vector<const char*> desiredInstanceExtensions;
        desiredInstanceExtensions.reserve(desiredExtensionsCount);
        for(uint32_t i = 0; i<desiredExtensionsCount;++i)
            desiredInstanceExtensions.push_back(glfwExtensions[i]);

        for(auto& extension : desiredInstanceExtensions){
            bool supported = false;
            for(auto& availableExtension : instanceExtensions){
                if(std::strcmp(extension, availableExtension.extensionName) == 0){
                    supported = true;
                    break;
                }
            }
            if(!supported){
                std::cerr << "Instance extension not supported" << std::endl;
                return false;
            }
        }

        VkApplicationInfo appInfo; // TODO: Get data from file
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pNext = nullptr;
        appInfo.pApplicationName = "AppName";
        appInfo.applicationVersion = VK_MAKE_VERSION(1,0,0);
        appInfo.pEngineName = "TheIcosahedron";
        appInfo.engineVersion = VK_MAKE_VERSION(1,0,0);
        appInfo.apiVersion = VK_MAKE_VERSION(1,0,0);

        VkInstanceCreateInfo instanceCreateInfo;
        instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instanceCreateInfo.pNext = nullptr;
        instanceCreateInfo.flags = 0;
        instanceCreateInfo.pApplicationInfo = &appInfo;
        instanceCreateInfo.enabledLayerCount = 0;
        instanceCreateInfo.ppEnabledLayerNames = nullptr;
        instanceCreateInfo.enabledExtensionCount = (uint32_t)desiredInstanceExtensions.size();
        instanceCreateInfo.ppEnabledExtensionNames = &desiredInstanceExtensions[0];

        if(vk::vkCreateInstance(&instanceCreateInfo, nullptr, &instance) != VK_SUCCESS){
            std::cerr << "Failed to create instance" << std::endl;
            return false;
        }

        if(!vk::LoadInstanceLevelFunctions(instance, desiredInstanceExtensions)){
            std::cout << "Failed to load instance level functions" << std::endl;
            return false;
        }

        return true;
    }

     bool Gfx::GetInstanceExtensions(std::vector<VkExtensionProperties>& instanceExtensions) {
        uint32_t extensionsCount = 0;

        if(vk::vkEnumerateInstanceExtensionProperties(nullptr, &extensionsCount, nullptr) != VK_SUCCESS){
            return false;
        }

        instanceExtensions.resize(extensionsCount);

        return vk::vkEnumerateInstanceExtensionProperties(nullptr, &extensionsCount, &instanceExtensions[0]) == VK_SUCCESS;
    }

    void Gfx::Terminate() {
        DeviceManager::Terminate();
        window.Terminate(instance);
        if(instance){
            vk::vkDestroyInstance(instance, nullptr);
            instance = VK_NULL_HANDLE;
        }
        vk::ReleaseLoader();
    }
}