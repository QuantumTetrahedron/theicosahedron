
#include <iostream>
#include <cstring>
#include "DeviceManager.h"

namespace TIH{
    std::vector<VkPhysicalDevice> DeviceManager::physicalDevices;
    VkDevice DeviceManager::logicalDevice;
    VkQueue DeviceManager::graphicsQueue;
    VkQueue DeviceManager::computeQueue;
    VkSwapchainKHR DeviceManager::swapchain = VK_NULL_HANDLE;

    bool DeviceManager::Initialize(const VkInstance &instance, const VkSurfaceKHR& surface) {
        if(!GetPhysicalDevices(instance)){
            std::cerr << "Failed to get physical devices" << std::endl;
            return false;
        }

        VkPhysicalDevice physicalDevice = physicalDevices[0];

        if(!CreateLogicalDevice(physicalDevice, surface)){
            return false;
        }

        if(!CreateSwapchain(physicalDevice, surface)){
            return false;
        }

        return true;
    }

    bool DeviceManager::GetDeviceNames(std::vector<const char *> &names) {
        // TODO

        return false;
    }

    bool DeviceManager::SetDevice(const char *deviceName) {
        // TODO

        /// Replace logical device with a new one
        /// Reload device level functions

        return false;
    }

    bool DeviceManager::GetPhysicalDevices(const VkInstance &instance) {
        uint32_t devicesCount = 0;
        if(vk::vkEnumeratePhysicalDevices(instance, &devicesCount, nullptr) != VK_SUCCESS){
            return false;
        }
        physicalDevices.resize(devicesCount);
        return vk::vkEnumeratePhysicalDevices(instance, &devicesCount, &physicalDevices[0]) == VK_SUCCESS;
    }

    bool DeviceManager::CreateLogicalDevice(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface) {
        std::vector<VkExtensionProperties> physicalDeviceExtensions;
        if(!GetDeviceExtensions(physicalDevice, physicalDeviceExtensions)){
            return false;
        }

        std::vector<const char*> desiredDeviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME }; // TODO: Get from file or something?
        for(auto& extension : desiredDeviceExtensions){
            bool supported = false;
            for(auto& availableExtension : physicalDeviceExtensions){
                if(std::strcmp(extension, availableExtension.extensionName) == 0){
                    supported = true;
                    break;
                }
            }
            if(!supported){
                std::cerr << "Device extension not supported" << std::endl;
                return false;
            }
        }

        VkPhysicalDeviceFeatures deviceFeatures;
        vk::vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);
        // TODO: Should only select needed features

        std::vector<QueueInfo> queueInfos;
        //if(!GetQueueInfos(physicalDevice, queueInfos)){
        //    return false;
        //}
        if(!GetQueuesToRequest(physicalDevice, surface, queueInfos)){
            return false;
        }


        std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
        for(const QueueInfo& info : queueInfos){
            VkDeviceQueueCreateInfo i;
            i.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            i.pNext = nullptr;
            i.flags = 0;
            i.queueFamilyIndex = info.FamilyIndex;
            i.queueCount = (uint32_t)info.Priorities.size();
            i.pQueuePriorities = &info.Priorities[0];
            queueCreateInfos.push_back(i);
        }

        VkDeviceCreateInfo deviceCreateInfo;
        deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCreateInfo.pNext = nullptr;
        deviceCreateInfo.flags = 0;
        deviceCreateInfo.queueCreateInfoCount = (uint32_t)queueCreateInfos.size();
        deviceCreateInfo.pQueueCreateInfos = &queueCreateInfos[0];
        deviceCreateInfo.enabledLayerCount = 0;
        deviceCreateInfo.ppEnabledLayerNames = nullptr;
        deviceCreateInfo.enabledExtensionCount = (uint32_t)desiredDeviceExtensions.size();
        deviceCreateInfo.ppEnabledExtensionNames = &desiredDeviceExtensions[0];
        deviceCreateInfo.pEnabledFeatures = &deviceFeatures;

        if(vk::vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &logicalDevice) != VK_SUCCESS){
            std::cout << "Failed to create logical device" << std::endl;
            return false;
        }

        if(!vk::LoadDeviceLevelFunctions(logicalDevice, desiredDeviceExtensions)){
            std::cout << "Failed to load device level functions" << std::endl;
            return false;
        }

        /// Not a very pretty piece of code. Will try to make it better someday
        if(queueInfos.size() == 2){
            vk::vkGetDeviceQueue(logicalDevice, queueInfos[0].FamilyIndex, 0, &graphicsQueue);
            vk::vkGetDeviceQueue(logicalDevice, queueInfos[1].FamilyIndex, 0, &computeQueue);
        }
        else if(queueInfos.size() == 1){
            vk::vkGetDeviceQueue(logicalDevice, queueInfos[0].FamilyIndex, 0, &graphicsQueue);
            computeQueue = graphicsQueue;
        }
        else return false;

        return true;
    }

    bool DeviceManager::GetDeviceExtensions(const VkPhysicalDevice &physicalDevice, std::vector<VkExtensionProperties> &availableExtensions) {
        uint32_t extensionsCount = 0;
        if(vk::vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionsCount, nullptr) != VK_SUCCESS){
            std::cerr << "Failed to get device extensions" << std::endl;
            return false;
        }
        availableExtensions.resize(extensionsCount);
        return vk::vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionsCount, &availableExtensions[0]) == VK_SUCCESS;
    }

    //bool DeviceManager::GetQueueInfos(const VkPhysicalDevice &physicalDevice, std::vector<DeviceManager::QueueInfo> &infos) {
    bool DeviceManager::GetQueuesToRequest(const VkPhysicalDevice &physicalDevice, const VkSurfaceKHR &surface,
                                           std::vector<TIH::DeviceManager::QueueInfo> &queues) {
        uint32_t queueFamiliesCount = 0;
        vk::vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamiliesCount, nullptr);
        if(queueFamiliesCount == 0){
            return false;
        }
        std::vector<VkQueueFamilyProperties> queueFamilies;
        queueFamilies.resize(queueFamiliesCount);
        vk::vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamiliesCount, &queueFamilies[0]);

        /// To request all queues
        /**
        for(uint32_t i = 0; i < queueFamilies.size(); ++i){
            QueueInfo info;
            info.FamilyIndex = i;
            for(uint32_t j = 0; j < queueFamilies[i].queueCount; ++j){
                info.Priorities.push_back(1.0f); // Are the priorities ever relevant enough?
            }
            queues.push_back(info);
        }*/

        /// To get only 2 specific queues
        auto graphicsQueueFamilyIndex = (uint32_t)queueFamilies.size();
        auto computeQueueFamilyIndex = (uint32_t)queueFamilies.size();

        for(uint32_t i = 0; i < queueFamilies.size(); ++i){
            if((queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)!=0){
                /// The graphics family should support image presentation
                VkBool32 presentationSupported;
                if(vk::vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentationSupported) != VK_SUCCESS || presentationSupported != VK_TRUE){
                    continue;
                }

                graphicsQueueFamilyIndex = i;
                break;
            }
        }

        for(uint32_t i = 0; i < queueFamilies.size(); ++i){
            if((queueFamilies[i].queueFlags & VK_QUEUE_COMPUTE_BIT)!=0){
                computeQueueFamilyIndex = i;
                break;
            }
        }

        if(graphicsQueueFamilyIndex >= queueFamilies.size()){
            std::cerr << "No graphics queues found" << std::endl;
            return false;
        }
        if(computeQueueFamilyIndex >= queueFamilies.size()){
            std::cerr << "No compute queue found" << std::endl;
            return false;
        }

        queues.push_back({graphicsQueueFamilyIndex, {1.0f}});
        if(graphicsQueueFamilyIndex != computeQueueFamilyIndex){
            queues.push_back({computeQueueFamilyIndex, {1.0f}});
        }

        return true;
    }

    void DeviceManager::Terminate() {
        if(logicalDevice){
            vk::vkDestroyDevice(logicalDevice, nullptr);
            logicalDevice = VK_NULL_HANDLE;
        }
    }

    bool DeviceManager::CreateSwapchain(const VkPhysicalDevice &physicalDevice, const VkSurfaceKHR &surface) {
        /// Selecting a presentation mode - I'd rather use VK_PRESENT_MODE_MAILBOX_KHR but my graphics card doesn't support it
        VkPresentModeKHR desiredPresentationMode = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
        uint32_t presentationModesCount;
        if(vk::vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentationModesCount, nullptr) != VK_SUCCESS){
            return false;
        }
        std::vector<VkPresentModeKHR> availablePresentationModes;
        availablePresentationModes.resize(presentationModesCount);
        if(vk::vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentationModesCount, &availablePresentationModes[0]) != VK_SUCCESS){
            return false;
        }

        bool presentModeSupported = false;
        for(auto mode : availablePresentationModes){
            /*if(mode == VK_PRESENT_MODE_FIFO_KHR){
                std::cout << "FIFO" << std::endl;
            } else if (mode == VK_PRESENT_MODE_FIFO_RELAXED_KHR){
                std::cout << "FIFO Relaxed" <<std::endl;
            } else if (mode == VK_PRESENT_MODE_IMMEDIATE_KHR){
                std::cout << "Intermediate" << std::endl;
            } else if (mode == VK_PRESENT_MODE_MAILBOX_KHR){
                std::cout << "Mailbox" << std::endl;
            }*/

            if(mode == desiredPresentationMode){
                presentModeSupported = true;
                break;
            }
        }
        if(!presentModeSupported){
            desiredPresentationMode = VK_PRESENT_MODE_FIFO_KHR; // Should be always awailable
        }

        /// Getting surface capabilities
        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        vk::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);
        //std::cout << surfaceCapabilities.minImageCount << " " << surfaceCapabilities.maxImageCount << std::endl;
        uint32_t numberOfImages = surfaceCapabilities.minImageCount+1;
        if(surfaceCapabilities.maxImageCount > 0){
            // There is a limit.
            if(surfaceCapabilities.maxImageCount < numberOfImages){
                numberOfImages = surfaceCapabilities.maxImageCount;
            }
        }
        VkExtent2D sizeOfImages;
        if(surfaceCapabilities.currentExtent.width == 0xFFFFFFFF){
            std::cout << "Window size defined by surface size" << std::endl;
            uint32_t width = 800;
            uint32_t height = 600;
            width = width > surfaceCapabilities.maxImageExtent.width ? surfaceCapabilities.maxImageExtent.width : width;
            width = width < surfaceCapabilities.minImageExtent.width ? surfaceCapabilities.minImageExtent.width : width;
            height = height > surfaceCapabilities.maxImageExtent.height ? surfaceCapabilities.maxImageExtent.height : height;
            height = height < surfaceCapabilities.minImageExtent.height ? surfaceCapabilities.minImageExtent.height : height;
            sizeOfImages.width = width;
            sizeOfImages.height = height;
        } else {
            sizeOfImages = surfaceCapabilities.currentExtent;
        }

        VkImageUsageFlags desiredUsages = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        desiredUsages &= surfaceCapabilities.supportedUsageFlags;

        VkSurfaceTransformFlagBitsKHR desiredTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        if(!(surfaceCapabilities.currentTransform & desiredTransform)){
            desiredTransform = surfaceCapabilities.currentTransform;
        }

        VkFormat colorFormat = VK_FORMAT_R8G8B8A8_UNORM;
        VkColorSpaceKHR imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
        VkSurfaceFormatKHR desiredSurfaceFormat;
        desiredSurfaceFormat.colorSpace = imageColorSpace;
        desiredSurfaceFormat.format = colorFormat;
        uint32_t formatsCount;
        vk::vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatsCount, nullptr);
        std::vector<VkSurfaceFormatKHR> surfaceFormats;
        surfaceFormats.resize(formatsCount);
        vk::vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatsCount, &surfaceFormats[0]);
        //std::cout << surfaceFormats.size() << std::endl;
        VkSurfaceFormatKHR surfaceFormat;
        if(surfaceFormats.size() == 1 && surfaceFormats[0].format == VK_FORMAT_UNDEFINED){
            // Any format is ok
            surfaceFormat = desiredSurfaceFormat;
        }else{
            // Check if desired surface format is available
            bool formatFound = false;
            for(auto& f : surfaceFormats){
                if(f.format == desiredSurfaceFormat.format && f.colorSpace == desiredSurfaceFormat.colorSpace){
                    surfaceFormat = desiredSurfaceFormat;
                    formatFound = true;
                    break;
                }
            }
            if(!formatFound){
                // Check if at least the color format is ok
                for(auto& f : surfaceFormats){
                    if(f.format == desiredSurfaceFormat.format){
                        surfaceFormat.format = desiredSurfaceFormat.format;
                        surfaceFormat.colorSpace = f.colorSpace;
                        formatFound = true;
                        break;
                    }
                }
                if(!formatFound){
                    // Give up and take any
                    surfaceFormat = surfaceFormats[0];
                }
            }
        }

        VkSwapchainKHR oldSwapchain = swapchain;

        VkSwapchainCreateInfoKHR swapchainCreateInfo;
        swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        swapchainCreateInfo.pNext = nullptr;
        swapchainCreateInfo.flags = 0;
        swapchainCreateInfo.surface = surface;
        swapchainCreateInfo.minImageCount = numberOfImages;
        swapchainCreateInfo.imageFormat = surfaceFormat.format;
        swapchainCreateInfo.imageColorSpace = surfaceFormat.colorSpace;
        swapchainCreateInfo.imageExtent = sizeOfImages;
        swapchainCreateInfo.imageArrayLayers = 1;
        swapchainCreateInfo.imageUsage = desiredUsages;
        swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swapchainCreateInfo.queueFamilyIndexCount = 0;
        swapchainCreateInfo.pQueueFamilyIndices = nullptr;
        swapchainCreateInfo.preTransform = desiredTransform;
        swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        swapchainCreateInfo.presentMode = desiredPresentationMode;
        swapchainCreateInfo.clipped = VK_TRUE;
        swapchainCreateInfo.oldSwapchain = oldSwapchain;

        if(vk::vkCreateSwapchainKHR(logicalDevice, &swapchainCreateInfo, nullptr, &swapchain) != VK_SUCCESS){
            swapchain = oldSwapchain;
            return false;
        }
        if(oldSwapchain != VK_NULL_HANDLE) {
            vk::vkDestroySwapchainKHR(logicalDevice, oldSwapchain, nullptr);
            oldSwapchain = VK_NULL_HANDLE;
        }
        return true;
    }
}