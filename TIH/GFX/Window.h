
#ifndef THEICOSAHEDRON_WINDOW_H
#define THEICOSAHEDRON_WINDOW_H

#include "VulkanLoader.h"
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

namespace TIH {
    class Window {
    public:
        void Init();
        bool shouldClose();
        void pollEvents();
        void Terminate(const VkInstance& instance);

        bool CreateSurface(const VkInstance& instance);

        const char** GetRequiredInstanceExtensions(uint32_t* extensionsCount);
        VkSurfaceKHR surface;
    private:
        GLFWwindow* window;
    };
}


#endif //THEICOSAHEDRON_WINDOW_H
