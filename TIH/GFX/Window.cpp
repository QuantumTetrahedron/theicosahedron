
#include "Window.h"

namespace TIH{

    void Window::Init() {
        glfwInit();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        window = glfwCreateWindow(800,600,"Vulkan",nullptr,nullptr);
    }

    bool Window::shouldClose() {
        return glfwWindowShouldClose(window);
    }

    void Window::pollEvents() {
        glfwPollEvents();
    }

    void Window::Terminate(const VkInstance& instance) {
        vk::vkDestroySurfaceKHR(instance, surface, nullptr);
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    const char **Window::GetRequiredInstanceExtensions(uint32_t *extensionsCount) {
        return glfwGetRequiredInstanceExtensions(extensionsCount);
    }

    bool Window::CreateSurface(const VkInstance &instance) {
        return glfwCreateWindowSurface(instance, window, nullptr, &surface) == VK_SUCCESS;
    }
}