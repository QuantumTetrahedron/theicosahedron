
#ifndef THEICOSAHEDRON_GFX_H
#define THEICOSAHEDRON_GFX_H

#include "VulkanLoader.h"
#include "Window.h"

namespace TIH {
    class Gfx {
    public:
        static bool Initialize();
        static void Terminate();
        // static void Draw();
        // static void RegisterComponent(RenderComponent* c);
        // ...
        static Window window;
    private:
        static bool CreateInstance();

        static bool GetInstanceExtensions(std::vector<VkExtensionProperties>& instanceExtensions);

        static VkInstance instance;
    };
}

#endif //THEICOSAHEDRON_GFX_H
