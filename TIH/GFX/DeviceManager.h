
#ifndef THEICOSAHEDRON_DEVICEMANAGER_H
#define THEICOSAHEDRON_DEVICEMANAGER_H

#include "VulkanLoader.h"
#include <vector>

namespace TIH {

    /// How to choose the device to use? Choose the first available and let the user change it later.
    class DeviceManager {
    public:
        static bool Initialize(const VkInstance& instance, const VkSurfaceKHR& surface);
        static bool GetDeviceNames(std::vector<const char*>& names);
        static bool SetDevice(const char* deviceName);
        static void Terminate();
    private:
        struct QueueInfo{
            uint32_t FamilyIndex;
            std::vector<float> Priorities;
        };

        static std::vector<VkPhysicalDevice> physicalDevices;
        static VkDevice logicalDevice;
        static VkQueue graphicsQueue;
        static VkQueue computeQueue;
        static VkSwapchainKHR swapchain;

        static bool GetPhysicalDevices(const VkInstance& instance);
        static bool GetDeviceExtensions(const VkPhysicalDevice& physicalDevice, std::vector<VkExtensionProperties>& availableExtensions);
        //static bool GetQueueInfos(const VkPhysicalDevice& physicalDevice, std::vector<QueueInfo>& infos);
        static bool GetQueuesToRequest(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface, std::vector<QueueInfo>& queues);
        static bool CreateSwapchain(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface);
        static bool CreateLogicalDevice(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface);
    };
}

#endif //THEICOSAHEDRON_DEVICEMANAGER_H
