#include "VulkanLoader.h"

#include <dlfcn.h>
#include <iostream>

namespace vk{
#define EXPORTED_VULKAN_FUNCTION( name ) PFN_##name name;
#define GLOBAL_LEVEL_VULKAN_FUNCTION( name ) PFN_##name name;
#define INSTANCE_LEVEL_VULKAN_FUNCTION( name ) PFN_##name name;
#define INSTANCE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) PFN_##name name;
#define DEVICE_LEVEL_VULKAN_FUNCTION( name ) PFN_##name name;
#define DEVICE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) PFN_##name name;

#include "VulkanLoader.inl"

#define EXPORTED_VULKAN_FUNCTION( name ) \
    name = (PFN_##name)dlsym( vulkan_library, #name); \
    if(!name){ \
    std::cout << "Could not load exported Vulkan function" << std::endl; \
    return false; \
    }

#define GLOBAL_LEVEL_VULKAN_FUNCTION( name ) \
    name = (PFN_##name)vkGetInstanceProcAddr( nullptr, #name ); \
    if(!name){ \
    std::cout << "Could not load global-level function" << std::endl; \
    return false; \
    }

    void* vulkan_library;
    bool LoadGlobalLevelFunctions(){
        vulkan_library = dlopen("libvulkan.so.1", RTLD_NOW);
        if(!vulkan_library) std::cout << "Elo error" << std::endl;
#include "VulkanLoader.inl"
        return true;
    }

#define INSTANCE_LEVEL_VULKAN_FUNCTION( name ) \
    name = (PFN_##name)vkGetInstanceProcAddr( instance, #name ); \
    if(!name){ \
    std::cout << "Could not load instance-level function" << std::endl; \
    return false; \
    }

#define INSTANCE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) \
    for( auto& enabled_extension : enabled_extensions ){ \
        if( std::string(enabled_extension) == std::string(extension) ) { \
            name = (PFN_##name)vkGetInstanceProcAddr( instance, #name ); \
            if(!name){ \
                std::cout << "Could not load instance-level function" << std::endl; \
                return false; \
            } \
            break; \
        } \
    }

    bool LoadInstanceLevelFunctions(const VkInstance& instance, const std::vector<char const *>& enabled_extensions){
#include "VulkanLoader.inl"
        return true;
    }

#define DEVICE_LEVEL_VULKAN_FUNCTION( name ) \
    name = (PFN_##name)vkGetDeviceProcAddr( device, #name ); \
    if(!name){ \
        std::cout << "Could not load device-level function" << std::endl; \
        return false; \
    }

#define DEVICE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) \
    for( auto& enabled_extension : enabled_extensions ){ \
        if(std::string(enabled_extension) == std::string(extension)){ \
            name = (PFN_##name)vkGetDeviceProcAddr( device, #name ); \
            if(!name){ \
                std::cout << "Could not load device-level function" << std::endl; \
                return false; \
            } \
        } \
    }

    bool LoadDeviceLevelFunctions(const VkDevice& device, const std::vector<const char*>& enabled_extensions){
#include "VulkanLoader.inl"
        return true;
    }

    void ReleaseLoader(){
        dlclose(vulkan_library);
        vulkan_library = nullptr;
    }
}
