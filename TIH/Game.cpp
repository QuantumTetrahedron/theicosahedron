
#include <iostream>
#include "Game.h"

namespace TIH {
    void Game::Initialize(const char *configFilePath) {
        if(!Gfx::Initialize()){
            std::cerr << "Graphics initialization failed" << std::endl;
        }
    }

    void Game::MainLoop() {
        while(!Gfx::window.shouldClose()){

            Gfx::window.pollEvents();
        }
    }

    void Game::Shutdown() {
        Gfx::Terminate();
    }
}
