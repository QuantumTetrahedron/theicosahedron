
#ifndef THEICOSAHEDRON_GAME_H
#define THEICOSAHEDRON_GAME_H

#include "GFX/Gfx.h"

namespace TIH {
    class Game {
    public:
        static void Initialize(const char *configFilePath);
        static void MainLoop();
        static void Shutdown();
    };
}


#endif //THEICOSAHEDRON_GAME_H
