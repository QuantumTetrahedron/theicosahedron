cmake_minimum_required(VERSION 3.13)
project(TheIcosahedron)

find_package(glfw3 REQUIRED)

set(CMAKE_CXX_STANDARD 17)

add_executable(TheIcosahedron main.cpp TIH/Game.cpp TIH/Game.h TIH/GFX/Gfx.cpp TIH/GFX/Gfx.h TIH/GFX/VulkanLoader.h TIH/GFX/VulkanLoader.cpp TIH/GFX/DeviceManager.cpp TIH/GFX/DeviceManager.h TIH/GFX/Window.cpp TIH/GFX/Window.h)
target_link_libraries(TheIcosahedron glfw dl)