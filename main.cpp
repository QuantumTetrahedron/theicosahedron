#include <iostream>

#include "TIH/Game.h"

int main() {
    TIH::Game::Initialize("config file path");
    TIH::Game::MainLoop();
    TIH::Game::Shutdown();

    return 0;
}